'use strict';

let employees = [{
        name: 'Piyush',
        email: 'abc@gmail.com',
        phone: '998899228899'
    },
    {
        name: 'Shubham',
        email: 'shubham@gmail.com',
        phone: '99865986729'
    },
    {
        name: 'Brijeshwar',
        email: 'brijeshwar@gmail.com',
        phone: '99883333299'
    },
    {
        name: 'Satish',
        email: 'saish@gmail.com',
        phone: '93334448899'
    },
    {
        name: 'Vinay',
        email: 'vinay@gmail.com',
        phone: '96476474299'
    },
    {
        name: 'Vishal',
        email: 'vishal@gmail.com',
        phone: '93536378899'
    }
];

let hotels = [{
        hotel: "Taj Chandigarh",
        address: "Block No. 9, Sector-17 A, Chandigarh 160017",
        lat: '30.7456',
        log: '76.7852'
    },
    {
        hotel: "JW Marriott Hotel Chandigarh",
        address: "Plot no: 6, Sector 35-B, Dakshin Marg Chandigarh, 160035 India",
        lat: '30.7267',
        log: '76.7668'
    },
    {
        hotel: "Hyatt Regency Chandigarh",
        address: "178 Industrial and Business Park Phase 1, Chandigarh, Chandigarh, India, 160002",
        lat: '30.7047',
        log: '76.8003'
    },
    {
        hotel: "The LaLiT Chandigarh",
        address: "The LaLiT, Rajiv Gandhi IT Park, Chandigarh (U.T), India, Pin - 160101",
        lat: '30.7307',
        log: '76.8414'
    }
];

let id = 0 ;

function cal(startJourney,endJourney) {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['dayGrid', 'interaction'],
        // validRange: {
        //     start: `${startJourney.getFullYear()}-${startJourney.getMonth()}-${startJourney.getDate()}`,
        //     end: `${endJourney.getFullYear()}-${endJourney.getMonth()}-${endJourney.getDate() + 1}`
        // },
        selectable: true,
        editable: true,
        selectMirror: true,
        select: function (arg) {
            
            if (new Date(startJourney).getTime() <= new Date(arg.start).getTime() && new Date(endJourney).getTime() >= new Date(arg.start).getTime()) {
                
                var title = prompt('Event Title:');
                if (title) {
                    id += 1;
                    calendar.addEvent({
                        editable: true,
                        id: id,
                        title: title,
                        start: arg.start,
                        end: arg.end,
                        allDay: arg.allDay
                    })
                }
                calendar.unselect()
            
            } else {
                alert("you are not selecting the correct date");
                calendar.unselect()
            }

            


            // var title = prompt('Event Title:');
            // if (title) {
            //     id += 1;
            //     calendar.addEvent({
            //         editable:true,
            //         id: id,
            //         title: title,
            //         start: arg.start,
            //         end: arg.end,
            //         allDay: arg.allDay
            //     })
            // }
            // calendar.unselect()
        },
        eventClick: function(info) {
            $('#editModal').modal('show');
            $('#editModal-input').val(info.event._def.title);
            $("#editModal .modal-footer button").on('click', function () {
                
                if ($(this).attr("id") == "editEvent") {
                    let newVal = $('#editModal-input').val();
                    
                   info.event.setProp( "title", newVal );
                   $("#editModal").modal('hide');
                } else if ($(this).attr("id") == "deleteEvent") {
                    var event = calendar.getEventById(info.event.id);
                    event.remove();
                    calendar.unselect()
                    $("#editModal").modal('hide');
                }
            });
           },
           dayRender:(e) => {
               if(new Date(startJourney) <= new Date(e.date) && new Date(endJourney) >= new Date(e.date)) {
                   e.el.classList.add('high');
                  
               }
           },
           eventDrop: (info) => {
           
            if(new Date(startJourney).getTime() <= new Date(info.event.start).getTime() && new Date(endJourney).getTime() >= new Date(info.event.start).getTime()) {

                return;
            }
            info.revert();
            setTimeout(() => {
                alert("you can drop it outside the highlighted dates")
            },500);
           }
           

    });
    calendar.render();

}

function initMap(lat, log) {
    var mapProp = {
        center: new google.maps.LatLng(lat, log),
        zoom: 13,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, log),
        map: map,
        animation: google.maps.Animation.BOUNCE
    });
}
$(document).ready(function () {
    // $(".body").hide();
    // cal();
    initMap('30.7047', '76.8003');
    let startDate;
    let endDate;
    $("#startDate").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: 0
    });
    $("#startDate").change(() => {
        let date = $("#startDate").datepicker("getDate");

        let startDate = new Date(date);
        let startyear = startDate.getFullYear();
        let startday = startDate.getDate();
        let startmonth = startDate.getMonth();
        $("#endDate").datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: new Date(startyear, startmonth, startday + 1)
        });
    })

    $("#selectEmployee").change(function () {

        employees.forEach(element => {
            if ($(this).val() == element.name) {
                $("#name").val(element.name)
                $("#email").val(element.email)
                $("#number").val(element.phone)
            }
        });
    });

    $("#selectHotel").change(function () {
        hotels.forEach(element => {
            if ($(this).val() == element.hotel) {
                $("#hotelAddress").val(element.address)
                initMap(element.lat, element.log)
            }
        });
    });

    $("#submit").click(function () {
        let hotel = $("#selectHotel").val();
        let employee = $("#selectEmployee").val();
        startDate = $("#startDate").val();
        endDate = $("#endDate").val();
        let startJourney = new Date(startDate);
        let endJourney = new Date(endDate);

        if ( employee == null || hotel == null || startDate == '' || endDate == '') {
            alert('Please fill all details');
            return false;
        }
        cal(startJourney,endJourney);
        $(".body").remove();
        // alert("Handler for .click() called.");
    })


})