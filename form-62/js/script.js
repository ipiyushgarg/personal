'use strict';

$(document).ready(function () {
    $(".display-detail").hide();
    $("#tabs").tabs();
    $("#tabs").tabs({
        disabled: [1, 2]
    });

    $("#form-one-prev").click(function () {
        return false;
    });
    $("#form-one-next").click(function () {
        let firstName = $("#first-name").val();
        let lastName = $("#last-name").val();
        
        var active = $("#tabs").tabs("option", "active");
        if (firstName.match(/[a-z]{1,10}/) && lastName.match(/[a-z]{1,10}/)) {
            
            $('#final-form-firstName').val(firstName);
            $('#final-form-lastName').val(lastName);
           
            $("#tabs").tabs("disable");
            $("#tabs").tabs("enable", active + 1);
            $("#tabs").tabs("option", "active", active + 1);
        } else {
            
            return false;
        }
    });
    $("#form-two-prev").click(function () {
        
        $('#first-name').val('');
        $('#last-name').val('');
        $("#tabs").tabs("disable");
        var active = $("#tabs").tabs("option", "active");
        $("#tabs").tabs("enable", active - 1);
        $("#tabs").tabs("option", "active", active - 1);
    });
    
    $("#form-two-next").click(function () {
        let country = $("#country").val();
        let state = $("#state").val();
        let city = $("#city").val();
        
        var active = $("#tabs").tabs("option", "active");
        if (country == '' && state == '' && city == '') {

            return false;
        } else {
            $('#final-form-country').val(country);
            $('#final-form-state').val(state);
            $('#final-form-city').val(city);
            
            $("#tabs").tabs("disable");
            $("#tabs").tabs("enable", active + 1);
            $("#tabs").tabs("option", "active", active + 1);
        }
    })
    $("#form-three-prev").click(function () {
        
        $('#country').val('');
        $('#state').val('');
        $('#city').val('');
        $("#tabs").tabs("disable");
        var active = $("#tabs").tabs("option", "active");
        $("#tabs").tabs("enable", active - 1);
        $("#tabs").tabs("option", "active", active - 1);
    });
    $("#form-three-next").click(function () {
        let qualification = $("#qualification").val();
        let startYear = $("#start-year").val();
        let endYear = $("#end-year").val();
        let schoolName = $("#school-name").val();
        var active = $("#tabs").tabs("option", "active");
        if (startYear.match(/\d{4,4}/) && endYear.match(/\d{4,4}/) && !(qualification == '') && !(schoolName == '')) {
          
            $('#final-form-qualification').val(qualification);
            $('#final-form-startYear').val(startYear);
            $('#final-form-endYear').val(endYear);
            $('#final-form-schoolName').val(schoolName);

            $("#tabs").hide();
            $(".display-detail").show();
        } else {
            return false;
           

        }
    })

})