'use strict';

function myMap(lat, log) {
    var mapCenter = {
        lat: lat,
        lng: log
    }

    var destCenter = {
        lat: lat,
        lng: log
    }

    var mapProp = {
        center: new google.maps.LatLng(lat, log),
        zoom: 13,
    };
    var input = document.getElementById("destination");
    input.addEventListener('blur', (res) => {
        res.preventDefault();
        res.stopPropagation();
        console.log(res.srcElement.value)
        console.log(res);
        var geo = new google.maps.Geocoder;
        geo.geocode({
            'address': res.srcElement.value
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var myLatLng = results[0].geometry.location;
                console.log(results[0].geometry.location)

            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    })
    var request = {
        location: mapCenter,
        radius: '500',
        type: ['atm']
    };

    function nearby() {
        
        let service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, (results, status) => {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    var place = results[i];
                    createMarker(results[i]);
                }
            }
        });

    }

    function createMarker(place) {
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }
    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer();

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    let autocomplete = new google.maps.places.Autocomplete(input);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, log),
        draggable: true,
        map: map,
        animation: google.maps.Animation.BOUNCE
    });


    nearby();
    directionsRenderer.setMap(map);

    var onChangeHandler = function () {
        calculateAndDisplayRoute(directionsService, directionsRenderer);
    };

    document.getElementById('currentLocation').addEventListener('change', onChangeHandler);
    document.getElementById('destination').addEventListener('change', onChangeHandler);

    function calculateAndDisplayRoute(directionsService, directionsRenderer) {
        directionsService.route({
                origin: {
                    query: document.getElementById('currentLocation').value
                },
                destination: {
                    query: document.getElementById('destination').value
                },
                travelMode: 'DRIVING'
            },
            function (response, status) {
                if (status === 'OK') {
                    directionsRenderer.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
    }

    var infowindow = new google.maps.InfoWindow();
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'location': mapCenter
    }, function (results, status) {
        if (status == 'OK') {

            marker.addListener('mouseover', function (res, err) {
                var address = results[0].formatted_address;
                infowindow.setContent(address);
                infowindow.open(map, marker);
            });
            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });

    google.maps.event.addListener(marker, "dragend", function (e) {

        mapCenter.lat = e.latLng.lat();
        mapCenter.lng = e.latLng.lng();
        nearby();
        geocoder.geocode({
            'location': mapCenter
        }, function (response, status) {
            if (status === "OK") {
                marker.addListener('mouseover', function (res, err) {
                    var address = response[0].formatted_address;
                    infowindow.setContent(address);
                    infowindow.open(map, marker);
                });
                marker.addListener('mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
        });
    });
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    myMap(position.coords.latitude, position.coords.longitude)
    $("#currentLocation").val(`${position.coords.latitude} , ${position.coords.longitude}`);

}
$(document).ready(function () {
    myMap(51.508742, -0.120850);
    getLocation();
    $("select").selectBoxIt();
})