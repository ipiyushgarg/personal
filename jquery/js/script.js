'use strict';

function previewImage(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#show-image').attr('src', e.target.result);

        }
        reader.readAsDataURL(input.files[0]);
    }
}
// function readURL(input) {
//     console.log(input.files);
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         reader.onload = function (e) {
//             $('#blah')
//                 .attr('src', e.target.result)
//                 .css({
//                     'width': '100%',
//                     'max-width': '750px',
//                     'height': '70%'
//                 })
//                 .width(50)
//                 .height(60);
//             var $image = $('#blah')
//             $image.cropper({
//                     aspectRatio: 16 / 9,
//                     rotatable: true,
//                     zoomable: true,
//                     scalable: true,
//                     crop: function (event) {}
//                 })
//                 //   var cropper = $image.data('cropper')
//                 .dialog({
//                     buttons: {
//                         "Crop": function () {
//                             console.log('ji')
//                             console.log(this)
//                             // Get a string base 64 data url
//                             // var croppedimage = new Image();
//                             // var cropBoxData = $image.cropper('getCropBoxData');
//                             // croppedimage = $image.data('cropper');
//                             let imageData = $('.cropper-canvas img').cropper('getCroppedCanvas').toDataURL();
//                             $(this).dialog("close");
//                             $('#blah').attr('src', imageData);

//                         },
//                         "Close": function () {
//                             $(this).dialog("close");
//                         },
//                         "Close": function () {
//                             $(this).dialog("close");
//                         },
//                         "Close": function () {
//                             $(this).dialog("close");
//                         },
//                         "Close": function () {
//                             $(this).dialog("close");
//                         }
//                     }
//                 });
//         };
//         reader.readAsDataURL(input.files[0]);
//     }
// }

let jsonData = [];
let id = 0;
$(document).ready(function () {

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        // contentHeight: 600,
        plugins: ['dayGrid', 'interaction'],
        selectable: true,

        selectMirror: true,
        select: function (arg) {
            var title = prompt('Event Title:');
            if (title) {
                id += 1;
                calendar.addEvent({
                    editable: true,
                    id: id,
                    title: title,
                    start: arg.start,
                    end: arg.end,
                    allDay: arg.allDay,
                })
            }

        },
        eventClick: function (info) {
            $('#editModal').modal('show');
            $('#editModal-input').val(info.event._def.title);
            $("#editModal .modal-footer button").on('click', function () {
                
                if ($(this).attr("id") == "editEvent") {
                    let newVal = $('#editModal-input').val();
                    
                   info.event.setProp( "title", newVal );
                   $("#editModal").modal('hide');
                } else if ($(this).attr("id") == "deleteEvent") {
                    var event = calendar.getEventById(info.event.id);
                    event.remove();
                    calendar.unselect()
                    $("#editModal").modal('hide');
                }
            });
            //    info.jsEvent.preventDefault(); // don't let the browser navigate

            //     if (info.event.id) {
            //         var event = calendar.getEventById(info.event.id);
            //         event.remove();
            //         calendar.unselect()
            //     }
        }

    });

    calendar.render();





    $.ajax({
        url: "https://jsonplaceholder.typicode.com/posts",

        success: function (data, err) {
            let json = JSON.parse(JSON.stringify(data))
            hey(json);


            // json.forEach((val, index) => {
            //     jsonData.push(val);
            //     $('.json-data').append('<div></div>')
            //     let div = $('.json-data').children('div')[index];
            //     $('<h3></h3>').addClass('user-id').text(val.id).appendTo(div)
            //     let h6 = $('.json-data').children('div')[index];
            //     $('<h6>ho</h6>').addClass('title').text(val.title).appendTo(div)
            //     let p = $('.json-data').children('div')[index]
            //     $('<p>ho</p>').text(val.body).appendTo(div)

            // })
        }
    });


    function hey(data) {

        for (const property in data) {
            jsonData.push(data[property]);
        }
    }

    let ind = 0;
    $(".render").mCustomScrollbar({
        theme: "dark",
        callbacks: {

            onTotalScroll: function () {
                console.log("Scrollbars d");
                let posts = jsonData.slice(0, 5);
                posts.forEach((val, index) => {
                    if (ind > 0) {
                        ind = ind + 1
                        $('.mCSB_container').append('<div></div>')
                        let div = $('.mCSB_container').children('div')[ind];
                        $('<h3></h3>').addClass('user-id').text(val.id).appendTo(div)
                        let h6 = $('.mCSB_container').children('div')[ind];
                        $('<h6>ho</h6>').addClass('title').text(val.title).appendTo(div)
                        let p = $('.mCSB_container').children('div')[ind]
                        $('<p>ho</p>').text(val.body).appendTo(div)
                    } else if (index == 4) {
                        // ind = ind + 1
                        jsonData.splice(0, 5);
                        console.log(jsonData)
                    } else {

                        $('.mCSB_container').append('<div></div>')
                        let div = $('.mCSB_container').children('div')[ind];
                        $('<h3></h3>').addClass('user-id').text(val.id).appendTo(div)
                        let h6 = $('.mCSB_container').children('div')[ind];
                        $('<h6>ho</h6>').addClass('title').text(val.title).appendTo(div)
                        let p = $('.mCSB_container').children('div')[ind]
                        $('<p>ho</p>').text(val.body).appendTo(div)
                    }




                })
            }
        }
    });

    $("#draggable").draggable();
    $("#droppable").droppable({
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p")
                .html("Dropped!");
        }
    });

    $("#choose-file").on("change", function () {
        previewImage(this);
        $("#edit-image").show();
    });
    $("#myModal").on("shown.bs.modal", function () {
        $('#edit-preview-image').cropper("destroy");
        $("#edit-preview-image").attr("src", $("#show-image").attr("src"));
        let myimg = $("#edit-preview-image");
        myimg.cropper({
            aspectRatio: 16 / 9,
            rotatable: true,
            zoomable: true,
            scalable: true,
        });
        console.log($(".cropper-container").attr("style"));
    });
    $("#rotate-45").on("click", function () {
        $("#edit-preview-image").cropper("rotate", "45");
    });
    $("#zoom-in").on("click", function () {
        $("#edit-preview-image").cropper("zoom", "0.1");
    });
    $("#zoom-out").on("click", function () {
        $("#edit-preview-image").cropper("zoom", "-0.1");
    });
    $("#scale-x").on("click", function () {
        let sc = $(this).data("scale");
        if (sc == 1) {
            $("#edit-preview-image").cropper("scaleX", "-1");
            $(this).data("scale", "-1");
        } else {
            $("#edit-preview-image").cropper("scaleX", "1");
            $(this).data("scale", "1");
        }
    });
    $("#scale-y").on("click", function () {
        let sc = $(this).data("scale");
        if (sc == 1) {
            $("#edit-preview-image").cropper("scaleY", "-1");
            $(this).data("scale", "-1");
        } else {
            $("#edit-preview-image").cropper("scaleY", "1");
            $(this).data("scale", "1");
        }
    });
    $("#reset").on("click", function () {
        $("#edit-preview-image").cropper("reset");
    });
    $("#save-image").on("click", function () {
        let imageData = $('#edit-preview-image').cropper('getCroppedCanvas').toDataURL();
        $('#show-image').attr('src', imageData);
        $("#myModal").modal("hide");
    });

    let num = 0;

    $('.buy-btn').click(function () {

        let product = [];
        let productTitle = $(this).parent().prev().text();
        let name = $(this).parent().children('ul').children('li').children('.item-1-name').text();
        let price = $(this).parent().children('ul').children('li').children('.item-1-price').text();
        let color = $(this).parent().children('ul').children('li').children('.item-1-color').text();
        product.push(productTitle, name, price, color);

        let child = $('tbody').append('<tr></tr>');
        let elementlen = $('tbody').children('tr').length;

        product.forEach(function (val, index) {
            if (index == 0) {

                $('tbody').children('tr').last().append('<th>' + elementlen + '</th>');
            } else {

                $('tbody').children('tr').last().append('<td>' + val + '</td>');
            }

        })
        $(this).hide();

        $(this).next().css('display', 'flex');

    })
    $('.remove-btn').click(function () {

        let productTitle = $(this).parent().prev().text();
        let name = $(this).parent().children('ul').children('li').children('.item-1-name').text();
        let price = $(this).parent().children('ul').children('li').children('.item-1-price').text();
        let color = $(this).parent().children('ul').children('li').children('.item-1-color').text();


        let listLength = $('tbody').children();
        let total = 0;
        let removeitem = 0;
        for (let i = 0; i < listLength.length; i++) {
            let x = $('tbody').children().eq(i).children('td').eq(0).text();

            if (x == name) {
                $('tbody').children().eq(i).remove();
                removeitem = i;
                console.log(i)
            } else if (removeitem < i) {}
        }
        $(this).hide();
        $(this).prev().css('display', 'flex');
    })


})