"use strict";



function checkForm(el) {

    if (el.id == 'fullName') {
        let fullName = el.value;

        // console.log( el.previousElementSibling);
        if (!fullName.match(/\w+\s\w+/i)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');
            //addSpan(el);

        }
    } else if (el.id == 'mobileNumber') {
        let mobileNumber = el.value;
        if (!mobileNumber.match(/\d{5}/)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');
            //addSpan(el);
        }
    } else if (el.id == 'inputEmail1') {
        let email = el.value;
        let emailTest = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email.match(emailTest)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');
            //addSpan(el);
        }
    } else if (el.id == 'inputPassword1') {
        let password = el.value;
        let passwordTest = /^(?=.*\d).{4,8}$/;
        if (!password.match(passwordTest)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');
            //addSpan(el);
        }
    }
}

function addSpan(el) {

    let s = document.createElement('span');
    s.innerHTML = '*';
    s.setAttribute('id', 'span');
    s.style.marginLeft = '4px';
    s.style.color = 'red';
    el.previousElementSibling.append(s);

}

function removeSpan(el) {
    //el.previousElementSibling.firstElementChild.remove();
    let label = el.previousElementSibling;
    let span = document.getElementById('span');

    if (label.contains(span)) {
        el.previousElementSibling.firstElementChild.remove();
    }
}

function removeValidate(el) {
    el.classList.remove('is-invalid');

    if (el.classList.contains('was-validated')) {
        el.classList.remove('was-validated');


    }
}

function checkEmail(el) {
    let email1 = document.getElementById('inputEmail1').value;


    if (el.value == '' || !(el.value == email1)) {
        console.log(email1 == el.value);
        el.classList.add('is-invalid');
        el.classList.add('was-validated');
    }
}

function checkPassword(el) {
    let email1 = document.getElementById('inputPassword1').value;
    if (el.value == '' || !(el.value === email1)) {
        el.classList.add('is-invalid');
        el.classList.add('was-validated');
    }
}

function register() {

    // let name = document.getElementById('fullName').value;
    // let mobNumber = document.getElementById('mobileNumber').value;
    // let email = document.getElementById('inputEmail1').value;
    // let password = document.getElementById('inputPassword1').value;


    let checkTag = ['fullName', 'mobileNumber', 'inputEmail1', 'inputPassword1'];
    let regExp = [/\w+\s\w+/i, /\d{5}/, /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, /^(?=.*\d).{4,8}$/];

    for (let i = 0; i < checkTag.length; i++) {

        let tag = document.getElementById(checkTag[i]);
        let tagvalue = tag.value;
        let validate = regExp[i];

        if (tag.id == 'inputEmail1') {
            let email1 = document.getElementById('inputEmail1');
            let email1value = email1.value;
            let email2 = document.getElementById('inputEmail2');
            let email2value = email2.value;
            if (!email1value.match(validate)) {
                email1.classList.add('is-invalid');
                email1.classList.add('was-validated');
            } else if (!(email1.value === email2value) || email2value == '') {
                email2.classList.add('is-invalid');
                email2.classList.add('was-validated');
            } else if (email1.value == '') {
                email1.classList.add('is-invalid');
                email1.classList.add('was-validated');
            } else {
                email1.classList.remove('is-invalid');
                email1.classList.remove('was-validated');
                email2.classList.remove('is-invalid');
                email2.classList.remove('was-validated');
            }
        } else if (tag.id == 'inputPassword1') {
            let password1 = document.getElementById('inputPassword1');
            let password1value = password1.value;
            let password2 = document.getElementById('inputPassword2');
            let password2value = password2.value;
            if (!password1value.match(validate)) {
                password1.classList.add('is-invalid');
                password1.classList.add('was-validated');
            } else if (!(password1.value === password2value) || password2value == '') {
                password2.classList.add('is-invalid');
                password2.classList.add('was-validated');
            } else if (password1.value == '') {
                password1.classList.add('is-invalid');
                password1.classList.add('was-validated');
            } else {
                password1.classList.remove('is-invalid');
                password1.classList.remove('was-validated');
                password2.classList.remove('is-invalid');
                password2.classList.remove('was-validated');
            }
        } else if (!tagvalue.match(validate)) {
            tag.classList.add('is-invalid');
            tag.classList.add('was-validated');
        }

    }


}