"use strict";

$(document).ready(() => {
    $( function() {
        $( "#slider-range" ).slider({
          range: true,
          min: 0,
          max: 365,
          values: [ 0, 365 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + "days"  + ui.values[ 1 ] + "days" );
          }
        });
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " days "
          + $( "#slider-range" ).slider( "values", 1 ) + " days" );
      } );

      $('#courses').chosen({no_results_text: "Oops, nothing found!"});
      
      $( "#date" ).datepicker( { minDate: new Date(Date.now()),
        beforeShowDay: function(date) {
            
            if ( date.getDay() == 2 || date.getDate() == 10 ) {
                return [true, "Highlighted"];
            }
            else {
                return [true, '', ''];
            }
        } } );

        $( "#date" ).datepicker("option", $.datepicker.regional[ "en-US" ] );
        $( "#locale" ).on( "change", function() {
            $( "#date" ).datepicker( "option",
              $.datepicker.regional[ $( this ).val() ] );
          });
    
});


let subjects = [];

function sub() {
    var checkBoxes = document.getElementsByClassName('checkbox');
    var subjects = [];
    for (var i = 0; i < checkBoxes.length; i++) {

        if (checkBoxes[i].checked) {
            subjects.push(checkBoxes[i].value);
        }

    }
    if (subjects.length >= 2) {

        document.getElementById('subject').style.display = 'none';
        document.getElementById('subject').classList.remove('is-invalid');
        document.getElementById('subject').classList.remove('was-validated');

    } else {

        document.getElementById('subject').style.display = 'block';
        document.getElementById('subject').classList.add('is-invalid');
        document.getElementById('subject').classList.add('was-validated');
        setTimeout(() => document.getElementById('subject').style.display = 'none', 2000)
    }

}

function addSubject(el) {
    if (el.checked) {
        sub();
        if (!(subjects.includes(el.value))) {
            subjects.push(el.value);
            document.getElementById('subjectsForm1').value = subjects;
        }
    } else if (!el.checked) {
        sub();
        if (subjects.includes(el.value)) {
            let pos = subjects.indexOf(el.value);
            subjects.splice(pos, 1);
            document.getElementById('subjectsForm1').value = subjects;
        }
    }
}

window.onload = function() {
    document.cookie = "name=piyush;expires=null";

}

function register() {
    document.cookie = "name=piyush;expires=null";
    // localStorage.setItem('abc','hello')
    // console.log('ll');
    let checkTag = ['firstName', 'lastName', 'mobileNumber', 'inputEmail1', 'address', 'courses', 'subjects', 'courseTime', 'date'];
    let regExp = [/^[a-z|A-Z]{3,10}$/i, /^[a-z|A-Z]{3,10}$/i, /((\+?)(91)([ -]?))(\d{0,3})([ -]?)(\d{0,3})([ -]?)(\d{0,3})([ -]?)(\d{0,1})/, /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/, /[a-zA-Z._^%$#!~@,-]{5,500}/];

    for (let i = 0; i < checkTag.length; i++) {

        let tag = document.getElementById(checkTag[i]);

        let validate = regExp[i];

        if (checkTag[i] == 'subjects') {
            sub();
        } else if (tag.id == 'firstName' || tag.id == 'lastName' || tag.id == 'mobileNumber' || tag.id == 'inputEmail1' ||  tag.id == 'address') {
            if (!(tag.value.match(validate))) {
                tag.classList.add('is-invalid');
                tag.classList.add('was-validated');
            }
        } else if (tag.id == 'courses' || tag.id == 'courseTime' || tag.id == 'date') {

            course(tag);
        }


    }

    console.log('hi')
    document.cookie = "name=piyush";
    //var name = document.getElementById('firstName').value;
    // document.cookie = "email=" + e - mail;
    // document.cookie = "pass=" + password;
}

function course(tag) {
    if (tag.value == '') {
        tag.classList.add('is-invalid');
        tag.classList.add('was-validated');
    } else {
        tag.classList.remove('is-invalid');
        tag.classList.remove('was-validated');
    }
}


function typeCheck(el) {

    if (el.id == 'firstName' || el.id == 'lastName') {
        let name = el.value;
        if (!name.match(/^[a-z|A-Z]{3,10}$/i)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');


        }
    } else if (el.id == 'inputEmail1') {
        let name = el.value;
        if (!name.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {

            el.classList.add('is-invalid');
            el.classList.add('was-validated');

        }
    } else if (el.id == 'mobileNumber') {
        let name = el.value;

        if (!name.match(/((\+?)(91)([ -]?))(\d{0,3})([ -]?)(\d{0,3})([ -]?)(\d{0,3})([ -]?)(\d{0,1})/)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');

        }
    } else if (el.id == 'address') {
        let name = el.value;

        if (!name.match(/[a-zA-Z._^%$#!~@,-]{5,500}/)) {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');

        }

    } else if (el.id == 'courses' || el.id == 'courseTime') {
        course(el);

    } else if (el.id == 'date') {
        if (el.value == '') {
            el.classList.add('is-invalid');
            el.classList.add('was-validated');
        } else {
            el.classList.remove('is-invalid');
            el.classList.remove('was-validated');
            document.getElementById('dateForm1').value = el.value;
        }
    }

}


function firstValue(el) {
    if (el.id == 'firstName') {
        document.getElementById('firstNameForm1').value = el.value;
    } else if (el.id == 'lastName') {
        document.getElementById('lastNameForm1').value = el.value;
    } else if (el.id == 'mobileNumber') {
        document.getElementById('phoneForm1').value = el.value;
    } else if (el.id == 'inputEmail1') {
        document.getElementById('emailForm1').value = el.value;
    } else if (el.id == 'address') {
        document.getElementById('addressForm1').value = el.value;
    } else if (el.id == 'courses') {
        document.getElementById('coursesForm1').value = el.value;
    } else if (el.id == 'date') {
        document.getElementById('dateForm1').value = el.value;
    }

}

function removeValidate(el) {
    el.classList.remove('is-invalid');
    el.classList.add('was-validated');

}
