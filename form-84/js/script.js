'use strict';

$(document).ready(function () {
    $(".display-detail").hide();
    $("#tabs").tabs();
    $("#tabs").tabs({
        disabled: [1, 2]
    });
    $( "#accordion" ).accordion();
    $("#number").mask('(+00) 000-000-0000');
    let dob;
    let d;
    let color;
    let fullName;
    let number;
    let startExp;
    let endExp;
    $( "#dob" ).datepicker({
        changeMonth: true,
      changeYear: true,
      yearRange: '1980:2030',
    });
    

    $("#form-one-prev").click(function () {
        return false;
    });

    $("#form-one-next").click(function () {
        fullName = $("#full-name").val();
        number = $("#number").val();
        dob = $("#dob").val();
        color = $("#color").val();
        d = new Date(dob);
        $( "#experience-start" ).datepicker({ changeMonth: true,
            changeYear: true,
            minDate: new Date(d.getFullYear() + 18 ,d.getMonth(), d.getDate())});
            $( "#experience-end" ).datepicker({ changeMonth: true,
                changeYear: true,
                minDate: new Date(d.getFullYear() + 18 ,d.getMonth(), d.getDate())});
            
        var active = $("#tabs").tabs("option", "active");
        if (fullName.match(/^([a-zA-Z0-9]+|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{1,}|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{3,}\s{1}[a-zA-Z0-9]{1,})$/) && number != '' && dob != '') {
            
           
            $("#tabs").tabs("disable");
            $("#tabs").tabs("enable", active + 1);
            $("#tabs").tabs("option", "active", active + 1);
        } else {
            return false;
        }
    });
    $("#form-two-prev").click(function () {
        
        $("#tabs").tabs("disable");
        var active = $("#tabs").tabs("option", "active");
        $("#tabs").tabs("enable", active - 1);
        $("#tabs").tabs("option", "active", active - 1);
    });
    
    $("#form-two-next").click(function () {
        startExp = $("#experience-start").val();
        endExp = $("#experience-end").val();
        var active = $("#tabs").tabs("option", "active");
        if (new Date(startExp) < new Date(endExp)) {

            $("#final-form-name").text(fullName);
            $("#final-form-number").text(number);
            $("#final-form-dob").text(dob);
            $("#final-form-color").text(color);
            $("#final-form-experience-start").text(startExp);
            $("#final-form-experience-end").text(endExp);

            $("#tabs").tabs("disable");
            $("#tabs").tabs("enable", active + 1);
            $("#tabs").tabs("option", "active", active + 1);
        } else {
            return false;
        }
        
    })
   

})